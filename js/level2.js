(function() {
    var game = new Phaser.Game(800, 600, Phaser.AUTO, '',
        {preload: preload, create: create, update: update});
    
    function preload(){
        game.load.image('background', 'assets/bg.jpg');

        //dude
        game.load.spritesheet('dude', 'assets/dude.png', 32, 48);
        
        //fan
        game.load.spritesheet('fan', 'assets/fan.png', 60, 50);
        
        //tube
        game.load.image('chemtube', 'assets/chemtube.png');
        
        //platform
        game.load.image('platform', 'assets/platform.png');
        
        //jumper
        game.load.spritesheet('jumper', 'assets/jumper.png', 14, 5);
        
        //chemical
        game.load.image('chemical', 'assets/chemical.png');
    }
    
    var platforms;
    var player;
    var jumpers;
    var fans;
    var tubes;
    var left_chems;
    var right_chems;
    var cursors;
    
    function create(){
        background = game.add.tileSprite(0, 0, 800, 600, 'background');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //setup the environment
        platforms = game.add.group();
        jumpers = game.add.group();
        fans = game.add.group();
        tubes = game.add.group();
        
        //adding tubes
        left_tube = game.add.sprite(275, 0, 'chemtube');
        left_tube.scale.setTo(4, 4);
        
        right_tube = game.add.sprite(525, 0, 'chemtube');
        right_tube.scale.setTo(4, 4);
        
        game.time.events.loop(Phaser.Timer.SECOND*3, spewChem, this);
        
        //adding platforms
        platforms.enableBody = true;
        
        topright_platform = platforms.create(650, 100, 'platform');
        topright_platform.body.immovable = true;
        ground = platforms.create(0, 550, 'platform');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;
        
        //add the dude
        player = game.add.sprite(50, 500, 'dude');
        game.physics.arcade.enable(player);
        player.body.gravity.y = 300;
        player.body.collideWorldBounds = true;
        player.animations.add('left', [0, 1, 2, 3], 12, true);
		player.animations.add('right', [5, 6, 7, 8], 12, true);
        player.frame = 4;
        
        //jumpers
        jumpers.enableBody = true;
        bot_jumper = jumpers.create(150, 530, 'jumper');
        midair_jumper = jumpers.create(400, 300, 'jumper');
        bot_jumper.body.immovable = true;
        midair_jumper.body.immovable = true;
        bot_jumper.scale.setTo(5, 5);
        midair_jumper.scale.setTo(5,5);
        
        //fans
        fans.enableBody = true;
        mid_fan = fans.create(0, 300, 'fan');
        mid_fan.body.immovable = true;
        mid_fan.frame = 0;
        
        top_fan = fans.create(0, 50, 'fan');
        top_fan.body.immovable = true;
        top_fan.frame = 0;
        
    }
    
    function update(){
        game.physics.arcade.collide(player, platforms, standardJump, null, this);
		game.physics.arcade.collide(player, jumpers, bigJump, null, this);
        if(left_chems)
            game.physics.arcade.overlap(player, left_chems, killPlayer, null, this);

        if(right_chems)
            game.physics.arcade.overlap(player, right_chems, killPlayer, null, this);

        //init frame
        player.body.velocity.x = 0;
        
        // player controls
		cursors = game.input.keyboard.createCursorKeys();

		if (cursors.left.isDown){
			player.body.velocity.x = -100;
			player.animations.play('left');
		}
		else if (cursors.right.isDown){
			player.body.velocity.x = 100;
			player.animations.play('right');
		}
		else {
			player.animations.stop();
			player.frame = 4;
		}
        
		// console.log(player.x, player.y);
		if (player.y >= 300 && player.y <= 350) {
			player.body.velocity.x += 450;
		}
        if(player.y >= 50 && player.y <= 100)
            player.body.velocity.x += 450;
    }
    
    
    function standardJump(player, platform){
		if (cursors.up.isDown && player.body.touching.down) {
			player.body.velocity.y = -150;
		}
	}

	// Fix: give bigJump only if jumping from on top of jumper
	// bug resolved by making sure that left and right is 
	//  not touching when its touching down
	function bigJump(player, jumper) {
		if (player.body.touching.down) {
			if (!player.body.touching.left && !player.body.touching.right) {
				player.body.velocity.y = -400;
				jumper.frame = 1;
			}
		}
	}
    
    function spewChem(){
        left_chems = game.add.sprite(289, 17, 'chemical');
        game.physics.arcade.enable(left_chems);
        left_chems.body.immovable = true;
        left_chems.scale.setTo(2.7, 17.7);
        
        right_chems = game.add.sprite(539, 17, 'chemical');
        game.physics.arcade.enable(right_chems);
        right_chems.body.immovable = true;
        right_chems.scale.setTo(2.7, 17.7);
        
        game.time.events.add(Phaser.Timer.SECOND*1, stopSpew, this);
    }
    
    function killPlayer(){
        player.destroy();
    }
    
    
    function stopSpew(){
        left_chems.destroy();
        right_chems.destroy();
    }

})();