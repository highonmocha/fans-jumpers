(function(){
	var game = new Phaser.Game(1000, 600, Phaser.AUTO, 'level3',
				{preload: preload, create: create, update:update});

	function preload(){
		// background
		game.load.image('background', 'assets/bg.jpg');

		// platform
		game.load.image('platform', 'assets/platform.png');
		
		// jumper
		game.load.spritesheet('jumper', 'assets/jumper.png', 14, 5);

		// dude
		game.load.spritesheet('dude', 'assets/dude.png', 32, 48);

		//fan
		game.load.spritesheet('fan', 'assets/fan.png', 60, 50);
	}

	var platforms;
	var player;
	var jumpers;
	var fans;
	var cursors;
	var airFlag = 0;

	function create(){
		// background
		background = game.add.tileSprite(0, 0, 1000, 600, 'background');

		game.physics.startSystem(Phaser.Physics.ARCADE);

		//setup the environment
		platforms = game.add.group();
		jumpers = game.add.group();
		fans = game.add.group();

		//ground and other boundaries
		platforms.enableBody = true;
		var ground = platforms.create(0, 550, 'platform');
		ground.body.immovable = true;
		ground.scale.setTo(3, 2);
		var left_brickwall = platforms.create(0, 50, 'platform');
		left_brickwall.body.immovable = true;
		left_brickwall.scale.setTo(1, 12.5);

		var right_brickwall = platforms.create(560, 50, 'platform');
		right_brickwall.body.immovable = true;
		right_brickwall.scale.setTo(0.5, 14);

		var destination = platforms.create(940, 300, 'platform');
		destination.body.immovable = true;
		
		//fans
		fans.enableBody = true;
		bottomright_fan = fans.create(505, 500, 'fan');
		bottomright_fan.body.immovable = true;
		topleft_fan = fans.create(0, 0, 'fan');
		topleft_fan.body.immovable = true;
		
		//many fans!
		fan1 = fans.create(950, 100, 'fan');
		fan1.body.immovable = true;
		fan2 = fans.create(950, 200, 'fan');
		fan2.body.immovable = true;

		game.time.events.loop(Phaser.Timer.SECOND*3, spewAir, this);

		//jumpers
		jumpers.enableBody = true;
		left_jumper = jumpers.create(400, 435, 'jumper');
		left_jumper.body.immovable = true;
		left_jumper.scale.setTo(4,4);

		right_jumper = jumpers.create(505, 235, 'jumper');
		right_jumper.body.immovable = true;
		right_jumper.scale.setTo(4,4);
		//player
		player = game.add.sprite(400, 500, 'dude');
		player.frame = 4;
		game.physics.arcade.enable(player);
		player.body.collideWorldBounds = true;
		player.body.gravity.y = 300;
		player.animations.add('left', [0, 1, 2, 3], 12, true);
		player.animations.add('right', [5, 6, 7, 8], 12, true);
	}

	function update(){
		game.physics.arcade.collide(player, platforms, standardJump, null, this);
		game.physics.arcade.collide(player, jumpers, bigJump, null, this);
		game.physics.arcade.collide(player, fans, standardJump, null, this);
		//start of every frame
		player.body.velocity.x = 0;

		//controls
		cursors = game.input.keyboard.createCursorKeys();

		if (cursors.left.isDown){
			player.body.velocity.x = -100;
			player.animations.play('left');
		}
		else if (cursors.right.isDown){
			player.body.velocity.x = 100;
			player.animations.play('right');
		}
		else {
			player.animations.stop();
			player.frame = 4;
		}

		if(airFlag == 1){
			if(player.y >= 0 && player.y <= 50)
				player.body.velocity.x += 300;
			if(player.y >= 500 && player.y <= 550)
				player.body.velocity.x -= 450;
			if(player.y >= 100 && player.y <= 150)
				player.body.velocity.x -= 450;
			if(player.y >= 200 && player.y <= 250)
				player.body.velocity.x -= 450;
		}
	}

	function spewAir(){
		airFlag = 1;
		game.time.events.add(Phaser.Timer.SECOND*1, stopAir, this);
	}

	function stopAir(){
		airFlag = 0;
	}

	function standardJump(player, platform){
		if (cursors.up.isDown && player.body.touching.down) {
			player.body.velocity.y = -200;
		}
	}

	// Fix: give bigJump only if jumping from on top of jumper
	// bug resolved by making sure that left and right is 
	//  not touching when its touching down
	function bigJump(player, jumper) {
		if (player.body.touching.down) {
			if (!player.body.touching.left && !player.body.touching.right) {
				player.body.velocity.y = -350;
				jumper.frame = 1;
			}
		}
	}
})();