(function() {
	var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game',
		{ preload: preload, create: create, update: update });

	var text;
	var cursors;
	function preload() {

	}

	function create() {
		text = game.add.text(400, 300, 'X', { fill: 'white'});
		game.physics.startSystem(Phaser.Physics.P2JS);
		game.physics.p2.enable(text);
		cursors = game.input.keyboard.createCursorKeys();

		// for the box
		var graphics = game.add.graphics(0, 0);
		graphics.lineStyle(3, 0x0000FF, 1);
		graphics.drawRect(0, 0, 400, 300);
	}

	function update() {

		if (cursors.left.isDown)
		{
			text.x -= 5;
		}
		else if (cursors.right.isDown)
		{
			text.x += 5;
		}

		if (cursors.up.isDown)
		{
			text.y -= 5;
		}
		else if (cursors.down.isDown)
		{
			text.y += 5;
		}

		// primitive force field implementation
		if (text.x <= 400 && text.y <= 300) {
			// if text moved in this area, give constant movement on +x
			text.x += 2;
		}
	}

})();
