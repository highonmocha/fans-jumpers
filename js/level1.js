(function() {
    var game = new Phaser.Game(800, 600, Phaser.AUTO, 'level1',
                { preload: preload, create: create, update: update});
    
    function preload(){
        game.load.image('background', 'assets/bg.jpg');
        game.load.spritesheet('dude', 'assets/dude.png', 32, 48);
        game.load.image('platform', 'assets/platform.png');
        game.load.spritesheet('jumper', 'assets/jumper.png', 14, 5);
    } 
    
    var platforms;
    var player;
    var jumpers;
    var cursors;
    
    function create(){
    	background = game.add.tileSprite(0, 0, 800, 600, 'background');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        platforms = game.add.group();
        platforms.enableBody = true;
        
        // ground
        var ground = platforms.create(0, 550, 'platform');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;
        
        // more platforms
        // left one
        var platform1 = platforms.create(-75, 100, 'platform');
        platform1.body.immovable = true;
        
        //right one
        var platform2 = platforms.create(650, 275, 'platform');
        platform2.body.immovable = true;
        
        // jumpers
        jumpers = game.add.group();
		jumpers.enableBody = true;
        
        
		jumper1 = jumpers.create(400, 530, 'jumper');
		jumper1.body.immovable = true;
		jumper1.scale.setTo(5, 5);
        
        jumper2 = jumpers.create(450, 180, 'jumper'); // mid air jumper
        jumper2.body.immovable = true;
        jumper2.scale.setTo(5, 5);
        
        // player 
        player = game.add.sprite(150, 500, 'dude');
		game.physics.arcade.enable(player);
		player.body.gravity.y = 300;
		player.body.collideWorldBounds = true;
		player.animations.add('left', [0, 1, 2, 3], 12, true);
		player.animations.add('right', [5, 6, 7, 8], 12, true);
        // facing front when starting.
        player.frame = 4;                             
        
    }
    
    function update(){
      game.physics.arcade.collide(player, platforms, standardJump, null, this);
      game.physics.arcade.collide(player, jumpers, bigJump, null, this);
        
        
        player.body.velocity.x = 0;
		
		// player controls
		cursors = game.input.keyboard.createCursorKeys();

		if (cursors.left.isDown){
			player.body.velocity.x = -150;
			player.animations.play('left');
		}
		else if (cursors.right.isDown){
			player.body.velocity.x = 150;
			player.animations.play('right');
		}
		else {
			player.animations.stop();
			player.frame = 4;
		}
	}

	function standardJump(player, platform){
		if (cursors.up.isDown && player.body.touching.down) {
			player.body.velocity.y = -250;
		}
	}

	// Fix: give bigJump only if jumping from on top of jumper
	// bug resolved by making sure that left and right is 
	//  not touching when its touching down
	function bigJump(player, jumper) {
		if (player.body.touching.down) {
			if (!player.body.touching.left && !player.body.touching.right) {
				player.body.velocity.y = -400;
				jumper.frame = 1;
			}
		}
	}
})();