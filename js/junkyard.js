(function() {
	var game = new Phaser.Game(800, 600, Phaser.AUTO, '', 
		{preload: preload, create: create, update: update});

	function preload(){
		game.load.image('platform', 'assets/platform.png');
		game.load.spritesheet('jumper', 'assets/jumper.png', 14, 5);
		game.load.spritesheet('dude', 'assets/dude.png', 32, 48);

		// button
		game.load.spritesheet('button', 'assets/button.png', 16, 29);
	}

	var jumper;
	var player;
	var platforms;
	var jumpers;
	var jumper1;

	function create(){
		game.physics.startSystem(Phaser.Physics.ARCADE);
		
		//setting up the environment
		platforms = game.add.group();
		platforms.enableBody = true;
		
		var ground = platforms.create(0, 550, 'platform');
		ground.body.immovable = true;
		ground.scale.setTo(2, 2);
		
		// the jumper
		jumpers = game.add.group();
		jumpers.enableBody = true;
		jumper1 = jumpers.create(40, 530, 'jumper');
		jumper1.body.immovable = true;
		jumper1.scale.setTo(5, 5);
		
		// setting up the player
		player = game.add.sprite(150, 500, 'dude');
		game.physics.arcade.enable(player);
		player.body.gravity.y = 300;
		player.body.collideWorldBounds = true;
		player.animations.add('left', [0, 1, 2, 3], 12, true);
		player.animations.add('right', [5, 6, 7, 8], 12, true);

		// button
		buttons = game.add.group();
		buttons.enableBody = true;
		button1 = buttons.create(420, 508, 'button');
		button1.body.immovable = true;
		button1.scale.setTo(1.5, 1.5);
		button1.frame = 1;
	}

	var cursors;

	function update(){
		game.physics.arcade.collide(player, platforms, standardJump, null, this);
		game.physics.arcade.collide(player, jumpers, bigJump, null, this);

		// collision with button
		game.physics.arcade.collide(player, buttons, buttonPress, null, this);
		
		//start of every frame
		player.body.velocity.x = 0;
		
		// player controls
		cursors = game.input.keyboard.createCursorKeys();

		if (cursors.left.isDown){
			player.body.velocity.x = -150;
			player.animations.play('left');
		}
		else if (cursors.right.isDown){
			player.body.velocity.x = 150;
			player.animations.play('right');
		}
		else {
			player.animations.stop();
			player.frame = 4;
		}
	}

	function standardJump(player, platform){
		if (cursors.up.isDown) {
			player.body.velocity.y = -150;
		}
	}

	// Fix: give bigJump only if jumping from on top of jumper
	// bug resolved by making sure that left and right is 
	//  not touching when its touching down
	function bigJump(player, jumper) {
		if (player.body.touching.down) {
			if (!player.body.touching.left && !player.body.touching.right) {
				player.body.velocity.y = -300;
				jumper.frame = 1;
			}
		}
	}

	// button activate / deactivate
	function buttonPress(playery, button) {
		// standard jump should work on button
		if (cursors.up.isDown) {
			if (player.body.touching.down) {
				player.body.velocity.y = -150;	
			}
		}

		// button activation / deactivation
		if (( player.body.touching.right && button.body.touching.left )
			|| ( player.body.touching.down && button.body.touching.up )
			|| ( player.body.touching.left && button.body.touching.right )) {
			if( game.input.keyboard.justPressed(Phaser.Keyboard.SPACEBAR, 10) ) {
				// console.log('button pressed!');
				button.frame = (button.frame + 1) % 2;
			}
		}
	}
})();