(function() {
	var game = new Phaser.Game(800, 600, Phaser.AUTO, '', 
		{preload: preload, create: create, update: update});

	function preload(){
		game.load.image('platform', 'assets/platform.png');
		game.load.spritesheet('jumper', 'assets/jumper.png', 14, 5);
		game.load.spritesheet('dude', 'assets/dude.png', 32, 48);

		// button
		game.load.spritesheet('button', 'assets/button.png', 16, 29);

		// fan
		game.load.spritesheet('fan', 'assets/fan.png', 60, 50);
		
		//chemical tube
		game.load.image('chemtube', 'assets/chemtube.png');
		game.load.image('chemical', 'assets/chemical.png');

		//standardJump sound
		game.load.audio('sjump', ['assets/jump_09.wav']);

		//bigJump sound
		game.load.audio('jjump', ['assets/jump_05.wav']);

		//fan sound
		game.load.audio('whoosh', ['assets/broken_fan.wav']);
	}

	var jumper;
	var player;
	var platforms;
	var jumpers;
	var jumper1;
	var chems;
	var jump_audio;
	var text;
	var count;

	function create(){
		game.physics.startSystem(Phaser.Physics.ARCADE);
		
		//setting up the environment
		platforms = game.add.group();
		platforms.enableBody = true;
		
		var ground = platforms.create(0, 550, 'platform');
		ground.body.immovable = true;
		ground.scale.setTo(2, 2);
		
		// the jumper
		jumpers = game.add.group();
		jumpers.enableBody = true;
		jumper1 = jumpers.create(250, 530, 'jumper');
		jumper1.body.immovable = true;
		jumper1.scale.setTo(5, 5);
		
		// jump audio
		sjump_audio = game.add.audio('sjump');
		sjump_audio.addMarker('sjump', 0, 1);

		jjump_audio = game.add.audio('jjump');
		jjump_audio.addMarker('jjump', 0, 1);

		// setting up the player
		player = game.add.sprite(0, 0, 'dude');
		game.physics.arcade.enable(player);
		player.body.gravity.y = 300;
		player.body.collideWorldBounds = true;
		player.animations.add('left', [0, 1, 2, 3], 12, true);
		player.animations.add('right', [5, 6, 7, 8], 12, true);

		// button
		buttons = game.add.group();
		buttons.enableBody = true;
		button1 = buttons.create(420, 508, 'button');
		button1.body.immovable = true;
		button1.scale.setTo(1.5, 1.5);
		button1.frame = 1;

		// fan
		fans = game.add.group();
		fans.enableBody = true;
		fan1 = fans.create(0, 300, 'fan');
		fan1.body.immovable = true;
		fan1.scale.setTo(2, 2);
		fan1.frame = 0;

		// link b/w button ^ fa
		button1.fan = fan1;
		
		// fan audio
		fan_audio = game.add.audio('whoosh');
		fan_audio.addMarker('whoosh', 1, 2);

		//chemical tube
		var tubes = game.add.group();
		var tube1 = tubes.create(550, 0, 'chemtube');
		tube1.scale.setTo(4,4);
		// spew chemicals after every 3 seconds
		game.time.events.loop(Phaser.Timer.SECOND*3, spewChem, this);

		count = 0;
		text = game.add.text(game.world.centerX, game.world.centerY, "0", 
							{	font: "18px Arial",
								fill: "#ff0044",
								align: "center"
							});
		// making the fan periodic
		game.time.events.loop(Phaser.Timer.SECOND*3, spewAir, this);
		// visual cue for the fan
		game.time.events.loop(Phaser.Timer.SECOND*1, showTime, this);
	}

	var cursors;
	var airFlag = 0;
	
	function update(){
		game.physics.arcade.collide(player, platforms, standardJump, null, this);
		game.physics.arcade.collide(player, jumpers, bigJump, null, this);
		if(chems)
			game.physics.arcade.overlap(player, chems, killPlayer, null, this);
	
		// collide with fan
		game.physics.arcade.collide(player, fans, standardJump, null, this);

		// collision with button
		game.physics.arcade.collide(player, buttons, buttonPress, null, this);
		
		//start of every frame
		player.body.velocity.x = 0;
		
		// player controls
		cursors = game.input.keyboard.createCursorKeys();

		if (cursors.left.isDown){
			player.body.velocity.x = -150;
			player.animations.play('left');
		}
		else if (cursors.right.isDown){
			player.body.velocity.x = 150;
			player.animations.play('right');
		}
		else {
			player.animations.stop();
			player.frame = 4;
		}

		if(airFlag == 1){
			if (player.y >= (300 - 48) && player.y <= 400) {
				player.body.velocity.x += 250;        // should switch to p2
													  // to use applyForce, cleaner
			}
		}
	}
	
	function showTime(){
		count += 1;
		text.setText(count%3);
	}

	function spewAir(){
		airFlag = 1;
		fan_audio.play('whoosh');
		game.time.events.add(Phaser.Timer.SECOND*1, stopAir, this);
	}

	function stopAir(){
		airFlag = 0;
	}

	function standardJump(player, platform){
		if (cursors.up.isDown && player.body.touching.down) {
			player.body.velocity.y = -150;
			sjump_audio.play('sjump');
		}
	}

	// Fix: give bigJump only if jumping from on top of jumper
	// bug resolved by making sure that left and right is 
	//  not touching when its touching down
	function bigJump(player, jumper) {
		if (player.body.touching.down) {
			if (!player.body.touching.left && !player.body.touching.right) {
				player.body.velocity.y = -400;
				jjump_audio.play('jjump');
				jumper.frame = 1;
			}
		}
	}

	// button activate / deactivate
	function buttonPress(playery, button) {
		// standard jump should work on button
		if (cursors.up.isDown) {
			if (player.body.touching.down) {
				player.body.velocity.y = -150;	
			}
		}

		// button activation / deactivation
		if (( player.body.touching.right && button.body.touching.left )
			|| ( player.body.touching.down && button.body.touching.up )
			|| ( player.body.touching.left && button.body.touching.right )) {
			if( game.input.keyboard.justPressed(Phaser.Keyboard.SPACEBAR, 10) ) {
				// console.log('button pressed!');
				button.frame = (button.frame + 1) % 2;
				button.fan.frame = (button.frame + 1) % 2;
			}
		}
	}
	
	function spewChem(){
		chems = game.add.sprite(565, 17, 'chemical');
		game.physics.arcade.enable(chems);
		chems.body.immovable = true;
		chems.scale.setTo(2.7, 17.7);
		game.time.events.add(Phaser.Timer.SECOND*1, stopSpew, this);
	}
	
	function killPlayer(){
		player.destroy();
	}
	
	
	function stopSpew(){
		chems.destroy();
	}
})();